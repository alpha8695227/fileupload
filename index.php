<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>

<?php

/**
 * Upload Form Submit
 */
if (isset($_POST['upload'])) {
    $file = $_FILES['photo'];


//get file information
 $file_name = rand() . $file['name'];
 $file_tmp_name = $file ['tmp_name'];
 $file_type = $file['type'];
 $file_size = $file['size']; 

// file Validation
if($file_type == 'image/jpeg'|| 
$file_type == 'image/jpg' || 
$file_type == 'image/JPG' || 
$file_type == 'image/JPEG' ||
$file_type == 'image/png' || 
$file_type == 'image/PNG' || 
$file_type == 'image/gif' || 
$file_type == 'image/GIF' &&
$file_size >= 200000
){

    move_uploaded_file($file_tmp_name, 'photo/' . $file_name);   

}else {
    $msg = "<p class = \"alert alert-danger\"> Invalid image file format.</p>";
}

}

?>
    <div class="container my-5">
        <div class="row justify-content-center">
            <div class="col-md-5">
                <div class="card shadow">
                    <div class="card-body">
                        <?php echo $msg ?? ''; ?>
                        <form action="" method ="POST" enctype="multipart/form-data">
                            <div class="my-3 file-content">
                                <input name="photo" type="file"id="fileUpload">
                                <label for="fileUpload"><img src="image icon.png" alt="image-icon"></label>
                            </div>
                            
                            <div class="card-image preview">
                                <img id="preview_photo" class="shadow" src="" alt="">
                            </div>
                            
                            <div class="my-3">
                                <input name="upload" class = "btn btn-sm btn-primary " type="submit" value="Upload Photo">
                            </div>
                        </form>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>


    <!--Jqurey cdn-->
    <script src="https://code.jquery.com/jquery-3.7.0.min.js"></script>
    <!--bootstrap cdn-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>


    <script>
    $('#fileUpload').change(function(e){
    $('#preview_photo').show();
    let file = URL.createObjectURL(e.target.files[0]);
    $('#preview_photo').attr('src', file);

    });

</script>

</body>
</html>